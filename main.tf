/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

terraform {
  backend "gcs" {
    bucket = "brunore-tfstate-orgops"
    prefix = "slo-export"
  }
}

locals {
  export_config = yamldecode(file("./configs/export/config.yaml"))
}

resource "google_service_account" "slo-generator-export" {
  project      = var.project_id
  account_id   = "slo-generator-export"
  display_name = "slo repo Service Account"
}

resource "google_bigquery_dataset" "export-dataset" {
  project                    = var.project_id
  dataset_id                 = "slos"
  friendly_name              = "slos"
  description                = "SLO Reports"
  location                   = "EU"
  delete_contents_on_destroy = true
  access {
    role          = "OWNER"
    user_by_email = google_service_account.slo-generator-export.email
  }
}

resource "google_project_iam_member" "bigquery-writer" {
  project = var.project_id
  role    = "roles/bigquery.dataEditor"
  member  = "serviceAccount:${google_service_account.slo-generator-export.email}"
}

module "slo-generator-export" {
  source                = "terraform-google-modules/slo/google//modules/slo-generator"
  version               = "3.0.0"
  service_name          = "slo-generator-export"
  target                = "run_export"
  signature_type        = "cloudevent"
  project_id            = var.project_id
  region                = var.region
  config                = local.export_config
  gcr_project_id        = var.gcr_project_id
  slo_generator_version = var.slo_generator_version
  service_account_email = google_service_account.slo-generator-export.email
  secrets = {
    EXPORT_PROJECT_ID          = var.project_id
    EXPORT_BIGQUERY_DATASET_ID = google_bigquery_dataset.export-dataset.dataset_id
  }
  authorized_members = var.authorized_members
  annotations        = var.annotations
}

resource "google_bigquery_table" "last_reports" {
  project     = var.project_id
  dataset_id  = google_bigquery_dataset.export-dataset.dataset_id
  table_id    = "last_reports"
  description = "Last compted service level achievements"
  view {
    use_legacy_sql = false
    query          = <<EOF
SELECT
   r2.*
FROM
   (
      SELECT
         r.service_name,
         r.feature_name,
         r.slo_name,
         r.window,
         MAX(r.timestamp_human) AS timestamp_human
      FROM
         `${var.project_id}.${google_bigquery_dataset.export-dataset.dataset_id}.reports` AS r
      GROUP BY
         r.service_name,
         r.feature_name,
         r.slo_name,
         r.window
      ORDER BY
         r.window
   )
   AS latest_report
   INNER JOIN
      `${var.project_id}.${google_bigquery_dataset.export-dataset.dataset_id}.reports` AS r2
      ON r2.service_name = latest_report.service_name
      AND r2.feature_name = latest_report.feature_name
      AND r2.slo_name = latest_report.slo_name
      AND r2.window = latest_report.window
      AND r2.timestamp_human = latest_report.timestamp_human
ORDER BY
   r2.service_name,
   r2.feature_name,
   r2.slo_name,
   r2.error_budget_policy_step_name
EOF
  }
}
