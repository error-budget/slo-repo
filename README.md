# slo-repo

Common repository to compute and store SLIs, SLOs and ErrorBudget

## slo iac roles

- BigQuery Admin
- Cloud run Admin
- Project IAM admin
- Secret Manager Admin
- Service Account Admin
- Service Account User
- Storage Admin

## Enabled APIs

- cloud run API
- identity and access management API
- resource manager API
- secret manager API
